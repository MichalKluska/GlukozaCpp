#pragma once
#include "IConsole.h"
#include <memory>

class Console : public IConsole
{
    Q_OBJECT

signals:
    void getData(const QByteArray & p_data);

public:
    explicit Console(QWidget *p_parent = nullptr);

    void putData(const QByteArray &p_data);

    void setLocalEchoEnabled(bool p_set);

protected:
    void keyPressEvent(QKeyEvent * p_keyEvent) Q_DECL_OVERRIDE;
    void mousePressEvent(QMouseEvent * p_mouseEvent) Q_DECL_OVERRIDE;
    void mouseDoubleClickEvent(QMouseEvent *p_mouseEvent) Q_DECL_OVERRIDE;
    void contextMenuEvent(QContextMenuEvent *p_contextEvent) Q_DECL_OVERRIDE;
private:
    bool localEchoEnabled;
    std::unique_ptr<QScrollBar> m_verticalScrollBar;
};

