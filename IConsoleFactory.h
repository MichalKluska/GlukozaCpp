#pragma once
#include "IConsole.h"

class IConsoleFactory
{
public:
    ~IConsoleFactory() = default;

    virtual std::unique_ptr<IConsole> createActiveConsole(QWidget *p_parent = nullptr) = 0;
    virtual std::unique_ptr<IConsole> createInactiveConsole() = 0;
};
