QT += widgets serialport
CONFIG += c++14

TARGET = GlukozaPomiar
TEMPLATE = app

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    Console.cpp \
    SettingsDialog.cpp \
    ConsoleInactive.cpp \
    ConsoleFactory.cpp \
    IDisplaySelector.cpp \
    qcustomplot.cpp \
    PlotFactory.cpp \
    PlotInactive.cpp

HEADERS += \
    IConsole.h \
    Console.h \
    ISettingsDialog.h \
    mainwindow.h \
    PortSettings.h \
    SettingsDialog.h \
    qcustomplot.h \
    IConsoleFactory.h \
    ConsoleFactory.h \
    IDisplaySelector.h \
    ConsoleFactory.h \
    ConsoleInactive.h \
    IPlotFactory.h \
    PlotFactory.h \
    PlotInactive.h


FORMS += \
    mainwindow.ui \
    settingsdialog.ui \
    mainwindow.ui \
    settingsdialog.ui




