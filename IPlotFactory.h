#pragma once
#include <memory>
#include "qcustomplot.h"

class IPlotFactory
{
public:
    ~IPlotFactory() = default;
    virtual std::unique_ptr<QCustomPlot> createQCustomPlot() = 0;
    virtual std::unique_ptr<QCustomPlot> createPlotInative() = 0;
};

