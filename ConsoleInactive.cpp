#include "ConsoleInactive.h"

void ConsoleInactive::putData(const QByteArray &)
{
}

void ConsoleInactive::setLocalEchoEnabled(bool)
{
}

void ConsoleInactive::keyPressEvent(QKeyEvent *)
{
}

void ConsoleInactive::mousePressEvent(QMouseEvent *)
{
}

void ConsoleInactive::mouseDoubleClickEvent(QMouseEvent *)
{
}

void ConsoleInactive::contextMenuEvent(QContextMenuEvent *)
{
}
