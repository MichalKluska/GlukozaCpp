#pragma once
#include "IConsoleFactory.h"
#include "Console.h"
#include "ConsoleInactive.h"

class IConsole;

class ConsoleFactory : public IConsoleFactory
{
public:
    ConsoleFactory() = default;
    std::unique_ptr<IConsole> createActiveConsole(QWidget *p_parent = nullptr) override;
    std::unique_ptr<IConsole> createInactiveConsole() override;
};

