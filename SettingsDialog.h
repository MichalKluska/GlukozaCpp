#pragma once
#include <memory>
#include <QDialog>
#include <QtSerialPort/QSerialPort>
#include "PortSettings.h"
#include "ISettingsDialog.h"

QT_USE_NAMESPACE

QT_BEGIN_NAMESPACE

namespace Ui {
class SettingsDialog;
}

class QIntValidator;
struct PortSettings;

QT_END_NAMESPACE

class SettingsDialog : public ISettingsDialog
{
    Q_OBJECT
public:
    explicit SettingsDialog(QWidget *parent = nullptr);
    ~SettingsDialog();

    PortSettings getSettings() const;

private slots:
    void showPortInfo(int p_index);
    void apply();
    void checkCustomBaudRatePolicy(int p_index);
    void checkCustomDevicePathPolicy(int p_index);

private:
    void initActionsConnections();
    void fillPortsParameters();
    void fillBaudRateOptions();
    void fillDataBitsOptions();
    void fillParityOptions();
    void fillStopBitsOptions();
    void fillFlowControlOptions();
    void fillPortsInfo();
    void updateSettings();
    void updateBaudRate();
    void updateDataBits();
    void updateParity();
    void updateStopBits();
    void updateFlowControl();
    void updateEcho();

    Ui::SettingsDialog* m_ui;
    PortSettings m_currentSettings;
    std::unique_ptr<QIntValidator> m_intValidator;
};

