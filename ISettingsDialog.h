#pragma once
#include <QDialog>
#include "PortSettings.h"

class ISettingsDialog : public QDialog
{
    Q_OBJECT
public:
    virtual PortSettings getSettings() const = 0;
};
