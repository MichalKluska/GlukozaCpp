#include "Console.h"

Console::Console(QWidget *p_parent)
    : localEchoEnabled(false)
    , m_verticalScrollBar(std::make_unique<QScrollBar>(verticalScrollBar()))
{
    document()->setMaximumBlockCount(100);
    QPalette l_pallette = palette();
    l_pallette.setColor(QPalette::Base, Qt::black);
    l_pallette.setColor(QPalette::Text, Qt::green);
    setPalette(l_pallette);
}

void Console::putData(const QByteArray &p_data)
{
    insertPlainText(QString(p_data));
    m_verticalScrollBar->setValue(m_verticalScrollBar->maximum());
}

void Console::setLocalEchoEnabled(bool p_set)
{
    localEchoEnabled = p_set;
}

void Console::keyPressEvent(QKeyEvent * p_keyEvent)
{
    if (localEchoEnabled)
    {
        QPlainTextEdit::keyPressEvent(p_keyEvent);
        emit getData(p_keyEvent->text().toLocal8Bit());
    }
}

void Console::mousePressEvent(QMouseEvent *p_mouseEvent)
{
    Q_UNUSED(p_mouseEvent);
    setFocus();
}

void Console::mouseDoubleClickEvent(QMouseEvent *p_mouseEvent)
{
    Q_UNUSED(p_mouseEvent);
}

void Console::contextMenuEvent(QContextMenuEvent *p_contextEvent)
{
    Q_UNUSED(p_contextEvent);
}
