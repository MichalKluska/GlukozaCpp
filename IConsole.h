#pragma once
#include <QPlainTextEdit>
#include <QScrollBar>
#include <memory>

class IConsole : public QPlainTextEdit
{
    Q_OBJECT

signals:
    virtual void getData(const QByteArray & p_data) = 0;

public:
    ~IConsole() = default;
    virtual void putData(const QByteArray &p_data) = 0;
    virtual void setLocalEchoEnabled(bool p_set) = 0;
    virtual void keyPressEvent(QKeyEvent * p_keyEvent) = 0;
    virtual void mousePressEvent(QMouseEvent * p_mouseEvent) = 0;
    virtual void mouseDoubleClickEvent(QMouseEvent *p_mouseEvent)  = 0;
    virtual void contextMenuEvent(QContextMenuEvent *p_contextEvent) = 0;
};
