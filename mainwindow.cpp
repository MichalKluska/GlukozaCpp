#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Console.h"
#include "ISettingsDialog.h"
#include <QMessageBox>
#include <QLabel>
#include <QtSerialPort/QSerialPort>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_ui(new Ui::MainWindow)
{
    createObjects();
    setupGUI();
    initActionsConnections();
}

MainWindow::~MainWindow()
{
    delete m_ui;
}

void MainWindow::openSerialPort()
{
    PortSettings l_settings = m_settings->getSettings();
    setPortParameters(l_settings);

    if (m_serial->open(QIODevice::ReadWrite))
    {
        performSuccessOpenPortOperations(l_settings);
    }
    else
    {
        performFailOpenPortOperations();
    }
}

void MainWindow::closeSerialPort()
{
    if (m_serial->isOpen())
    {
        m_serial->close();
    }
    performClosePortOperations();
}

void MainWindow::writeData(const QByteArray &p_data)
{
    m_serial->write(p_data);
}

void MainWindow::readData()
{
    QByteArray l_data = m_serial->readAll();
    m_data.append(l_data);
    m_console->putData(l_data);
}

void MainWindow::handleError(const QSerialPort::SerialPortError& p_error)
{
    if (p_error == QSerialPort::ResourceError)
    {
        QMessageBox::critical(this, tr("Critical Error"), m_serial->errorString());
        closeSerialPort();
    }
}

void MainWindow::createObjects()
{
    m_consoleFactory = std::make_unique<ConsoleFactory>();
    m_plotFactory = std::make_unique<PlotFactory> ();
    m_serial = std::make_unique<QSerialPort>(new QSerialPort(this));
    m_settings = std::make_unique<SettingsDialog>(new SettingsDialog(this));
    m_status = std::make_unique<QLabel>(new QLabel);
    m_console = m_consoleFactory->createActiveConsole();
    m_customPlot = m_plotFactory->createPlotInative();
}

void MainWindow::setupGUI()
{
    m_ui->setupUi(this);
    setCentralWidget(m_console.get());
    m_ui->actionConnect->setEnabled(true);
    m_ui->actionDisconnect->setEnabled(false);
    m_ui->actionQuit->setEnabled(true);
    m_ui->actionConfigure->setEnabled(true);
    m_ui->statusBar->addWidget(m_status.get());
}

void MainWindow::initActionsConnections()
{
    connect(m_ui->actionConnect, &QAction::triggered, this, &MainWindow::openSerialPort);
    connect(m_ui->actionDisconnect, &QAction::triggered, this, &MainWindow::closeSerialPort);
    connect(m_ui->actionQuit, &QAction::triggered, this, &MainWindow::close);
    connect(m_ui->actionConfigure, &QAction::triggered, m_settings.get(), &MainWindow::show);
    connect(m_ui->actionClear, &QAction::triggered, m_console.get(), &Console::clear);
    connect(m_ui->actionSwitch_Console_Plot, &QAction::triggered, this, &MainWindow::switchPlotConsole);
    connect(m_serial.get(), static_cast<void (QSerialPort::*)(QSerialPort::SerialPortError)>(&QSerialPort::error),
            this, &MainWindow::handleError);
    connect(m_serial.get(), &QSerialPort::readyRead, this, &MainWindow::readData);
    connect(static_cast<Console*>(m_console.get()), &Console::getData, this, &MainWindow::writeData);

}

void MainWindow::setPortParameters(const PortSettings& p_settings)
{
    m_serial->setPortName(p_settings.name);
    m_serial->setBaudRate(p_settings.baudRate);
    m_serial->setDataBits(p_settings.dataBits);
    m_serial->setParity(p_settings.parity);
    m_serial->setStopBits(p_settings.stopBits);
    m_serial->setFlowControl(p_settings.flowControl);
}

void MainWindow::performSuccessOpenPortOperations(const PortSettings& p_settings)
{
    m_console->setEnabled(true);
    m_console->setLocalEchoEnabled(p_settings.localEchoEnabled);
    m_ui->actionConnect->setEnabled(false);
    m_ui->actionDisconnect->setEnabled(true);
    m_ui->actionConfigure->setEnabled(false);
    showStatusMessage(tr("Connected to %1 : %2, %3, %4, %5, %6")
                      .arg(p_settings.name).arg(p_settings.stringBaudRate).arg(p_settings.stringDataBits)
                      .arg(p_settings.stringParity).arg(p_settings.stringStopBits).arg(p_settings.stringFlowControl));
}

void MainWindow::performFailOpenPortOperations()
{
    QMessageBox::critical(this, tr("Error"), m_serial->errorString());
    showStatusMessage(tr("Open error"));
}

void MainWindow::performClosePortOperations()
{
    m_console->setEnabled(false);
    m_ui->actionConnect->setEnabled(true);
    m_ui->actionDisconnect->setEnabled(false);
    m_ui->actionConfigure->setEnabled(true);
    showStatusMessage(tr("Disconnected"));
}

void MainWindow::showStatusMessage(const QString &p_message)
{
    m_status->setText(p_message);
}

void MainWindow::switchPlotConsole()
{
    static bool s_isConsoleActive = true;
    s_isConsoleActive = !s_isConsoleActive;
    if( s_isConsoleActive == true)
    {
        m_console = m_consoleFactory->createActiveConsole();
        m_customPlot = m_plotFactory->createPlotInative();
        setCentralWidget(m_console.get());
    }
    else
    {
        m_console = m_consoleFactory->createInactiveConsole();
        m_customPlot = m_plotFactory->createQCustomPlot();
        setCentralWidget(m_customPlot.get());
    }
}
