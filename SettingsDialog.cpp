#include "SettingsDialog.h"
#include "ui_settingsdialog.h"
#include <QtSerialPort/QSerialPortInfo>
#include <QIntValidator>
#include <QLineEdit>

QT_USE_NAMESPACE

static const char blankString[] = QT_TRANSLATE_NOOP("SettingsDialog", "N/A");

SettingsDialog::SettingsDialog(QWidget*) :
    m_ui(new Ui::SettingsDialog),
    m_intValidator(std::make_unique<QIntValidator>(new QIntValidator(0, 4000000, this)))
{
    m_ui->setupUi(this);

    initActionsConnections();
    fillPortsParameters();
    fillPortsInfo();

    updateSettings();
}

SettingsDialog::~SettingsDialog()
{
    delete m_ui;
}

PortSettings SettingsDialog::getSettings() const
{
    return m_currentSettings;
}

void SettingsDialog::showPortInfo(int p_index)
{
    if (p_index == -1)
    {
        return;
    }

    QStringList list = m_ui->serialPortInfoListBox->itemData(p_index).toStringList();
    m_ui->descriptionLabel->setText(tr("Description: %1").arg(list.count() > 1 ? list.at(1) : tr(blankString)));
    m_ui->manufacturerLabel->setText(tr("Manufacturer: %1").arg(list.count() > 2 ? list.at(2) : tr(blankString)));
    m_ui->serialNumberLabel->setText(tr("Serial number: %1").arg(list.count() > 3 ? list.at(3) : tr(blankString)));
    m_ui->locationLabel->setText(tr("Location: %1").arg(list.count() > 4 ? list.at(4) : tr(blankString)));
    m_ui->vidLabel->setText(tr("Vendor Identifier: %1").arg(list.count() > 5 ? list.at(5) : tr(blankString)));
    m_ui->pidLabel->setText(tr("Product Identifier: %1").arg(list.count() > 6 ? list.at(6) : tr(blankString)));
}

void SettingsDialog::apply()
{
    updateSettings();
    hide();
}

void SettingsDialog::checkCustomBaudRatePolicy(int p_index)
{
    bool l_isCustomBaudRate = !m_ui->baudRateBox->itemData(p_index).isValid();
    m_ui->baudRateBox->setEditable(l_isCustomBaudRate);

    if (l_isCustomBaudRate)
    {
        m_ui->baudRateBox->clearEditText();
        std::unique_ptr<QLineEdit> l_edit = std::make_unique<QLineEdit> (m_ui->baudRateBox->lineEdit());
        l_edit.get()->setValidator(m_intValidator.get());
    }
}

void SettingsDialog::checkCustomDevicePathPolicy(int p_index)
{
    bool l_isCustomPath = !m_ui->serialPortInfoListBox->itemData(p_index).isValid();
    m_ui->serialPortInfoListBox->setEditable(l_isCustomPath);

    if (l_isCustomPath)
    {
        m_ui->serialPortInfoListBox->clearEditText();
    }
}

void SettingsDialog::initActionsConnections()
{
    connect(m_ui->applyButton, &QPushButton::clicked,
            this, &SettingsDialog::apply);
    connect(m_ui->serialPortInfoListBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this, &SettingsDialog::showPortInfo);
    connect(m_ui->baudRateBox,  static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this, &SettingsDialog::checkCustomBaudRatePolicy);
    connect(m_ui->serialPortInfoListBox,  static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this, &SettingsDialog::checkCustomDevicePathPolicy);
}

void SettingsDialog::fillPortsParameters()
{
    fillBaudRateOptions();
    fillDataBitsOptions();
    fillParityOptions();
    fillStopBitsOptions();
    fillFlowControlOptions();
}

void SettingsDialog::fillPortsInfo()
{
    m_ui->serialPortInfoListBox->clear();
    QString l_description;
    QString l_manufacturer;
    QString l_serialNumber;
    QStringList l_list;
    const auto l_infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : l_infos)
    {
        l_description = info.description();
        l_manufacturer = info.manufacturer();
        l_serialNumber = info.serialNumber();
        l_list << info.portName()
               << (!l_description.isEmpty()  ? l_description : blankString)
               << (!l_manufacturer.isEmpty() ? l_manufacturer : blankString)
               << (!l_serialNumber.isEmpty() ? l_serialNumber : blankString)
               << info.systemLocation()
               << (info.vendorIdentifier() ? QString::number(info.vendorIdentifier(), 16) : blankString)
               << (info.productIdentifier() ? QString::number(info.productIdentifier(), 16) : blankString);

        m_ui->serialPortInfoListBox->addItem(l_list.first(), l_list);
    }

    m_ui->serialPortInfoListBox->addItem(tr("Custom"));
}

void SettingsDialog::updateSettings()
{
    m_currentSettings.name = m_ui->serialPortInfoListBox->currentText();

    updateBaudRate();
    updateDataBits();
    updateParity();
    updateStopBits();
    updateFlowControl();
    updateEcho();
}

void SettingsDialog::fillBaudRateOptions()
{
    m_ui->baudRateBox->addItem(QStringLiteral("9600"), QSerialPort::Baud9600);
    m_ui->baudRateBox->addItem(QStringLiteral("19200"), QSerialPort::Baud19200);
    m_ui->baudRateBox->addItem(QStringLiteral("38400"), QSerialPort::Baud38400);
    m_ui->baudRateBox->addItem(QStringLiteral("115200"), QSerialPort::Baud115200);
    m_ui->baudRateBox->addItem(tr("Custom"));
}

void SettingsDialog::fillDataBitsOptions()
{
    m_ui->dataBitsBox->addItem(QStringLiteral("5"), QSerialPort::Data5);
    m_ui->dataBitsBox->addItem(QStringLiteral("6"), QSerialPort::Data6);
    m_ui->dataBitsBox->addItem(QStringLiteral("7"), QSerialPort::Data7);
    m_ui->dataBitsBox->addItem(QStringLiteral("8"), QSerialPort::Data8);
    m_ui->dataBitsBox->setCurrentIndex(3);
}

void SettingsDialog::fillParityOptions()
{
    m_ui->parityBox->addItem(tr("None"), QSerialPort::NoParity);
    m_ui->parityBox->addItem(tr("Even"), QSerialPort::EvenParity);
    m_ui->parityBox->addItem(tr("Odd"), QSerialPort::OddParity);
    m_ui->parityBox->addItem(tr("Mark"), QSerialPort::MarkParity);
    m_ui->parityBox->addItem(tr("Space"), QSerialPort::SpaceParity);
}

void SettingsDialog::fillStopBitsOptions()
{
    m_ui->stopBitsBox->addItem(QStringLiteral("1"), QSerialPort::OneStop);

    #ifdef Q_OS_WIN
    m_ui->stopBitsBox->addItem(tr("1.5"), QSerialPort::OneAndHalfStop);
    #endif

    m_ui->stopBitsBox->addItem(QStringLiteral("2"), QSerialPort::TwoStop);
}

void SettingsDialog::fillFlowControlOptions()
{
    m_ui->flowControlBox->addItem(tr("None"), QSerialPort::NoFlowControl);
    m_ui->flowControlBox->addItem(tr("RTS/CTS"), QSerialPort::HardwareControl);
    m_ui->flowControlBox->addItem(tr("XON/XOFF"), QSerialPort::SoftwareControl);
}

void SettingsDialog::updateBaudRate()
{
    if (m_ui->baudRateBox->currentIndex() == 4)
    {
        m_currentSettings.baudRate = m_ui->baudRateBox->currentText().toInt();
    }
    else
    {
        m_currentSettings.baudRate = static_cast<QSerialPort::BaudRate>(
                    m_ui->baudRateBox->itemData(m_ui->baudRateBox->currentIndex()).toInt());
    }
    m_currentSettings.stringBaudRate = QString::number(m_currentSettings.baudRate);
}

void SettingsDialog::updateDataBits()
{
    m_currentSettings.dataBits = static_cast<QSerialPort::DataBits>(
                m_ui->dataBitsBox->itemData(m_ui->dataBitsBox->currentIndex()).toInt());
    m_currentSettings.stringDataBits = m_ui->dataBitsBox->currentText();
}

void SettingsDialog::updateParity()
{
    m_currentSettings.parity = static_cast<QSerialPort::Parity>(
                m_ui->parityBox->itemData(m_ui->parityBox->currentIndex()).toInt());
    m_currentSettings.stringParity = m_ui->parityBox->currentText();
}

void SettingsDialog::updateStopBits()
{
    m_currentSettings.stopBits = static_cast<QSerialPort::StopBits>(
                m_ui->stopBitsBox->itemData(m_ui->stopBitsBox->currentIndex()).toInt());
    m_currentSettings.stringStopBits = m_ui->stopBitsBox->currentText();
}

void SettingsDialog::updateFlowControl()
{
    m_currentSettings.flowControl = static_cast<QSerialPort::FlowControl>(
                m_ui->flowControlBox->itemData(m_ui->flowControlBox->currentIndex()).toInt());
    m_currentSettings.stringFlowControl = m_ui->flowControlBox->currentText();
}

void SettingsDialog::updateEcho()
{
    m_currentSettings.localEchoEnabled = m_ui->localEchoCheckBox->isChecked();
}
