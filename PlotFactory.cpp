#include "PlotFactory.h"

std::unique_ptr<QCustomPlot> PlotFactory::createQCustomPlot()
{
    return std::make_unique<QCustomPlot>();
}

std::unique_ptr<QCustomPlot> PlotFactory::createPlotInative()
{
    return std::make_unique<PlotInactive>();
}
