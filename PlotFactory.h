#pragma once
#include "IPlotFactory.h"
#include "qcustomplot.h"
#include "PlotInactive.h"

class QCustomPlot;

class PlotFactory : public IPlotFactory
{
public:
    PlotFactory() = default;
    std::unique_ptr<QCustomPlot> createQCustomPlot() override;
    std::unique_ptr<QCustomPlot> createPlotInative() override;
};
