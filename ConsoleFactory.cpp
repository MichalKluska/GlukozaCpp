#include "ConsoleFactory.h"

std::unique_ptr<IConsole> ConsoleFactory::createActiveConsole(QWidget *p_parent)
{
    return std::make_unique<Console>(new Console(p_parent));
}

std::unique_ptr<IConsole> ConsoleFactory::createInactiveConsole()
{
    return std::make_unique<ConsoleInactive>();
}
