#pragma once
#include <QMainWindow>
#include "IConsole.h"
#include "ISettingsDialog.h"
#include "SettingsDialog.h"
#include "qcustomplot.h"
#include "PlotInactive.h"
#include "PlotFactory.h"
#include "ConsoleFactory.h"
#include <QtSerialPort/QSerialPort>
#include <QtCore/QtGlobal>
#include <memory>

QT_BEGIN_NAMESPACE

class QLabel;

namespace Ui {
class MainWindow;
}

QT_END_NAMESPACE

class IConsoleFactory;
class IPlotFactory;
class IConsole;
class ISettingsDialog;
class PortSettings;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void openSerialPort();
    void closeSerialPort();
    void writeData(const QByteArray &p_data);
    void readData();
    void handleError(const QSerialPort::SerialPortError &p_error);

private:
    void createObjects();
    void setupGUI();
    void initActionsConnections();
    void setPortParameters(const PortSettings& p_settings);
    void performSuccessOpenPortOperations(const PortSettings& p_settings);
    void performFailOpenPortOperations();
    void performClosePortOperations();
    void showStatusMessage(const QString &p_message);
    void switchPlotConsole();

    Ui::MainWindow* m_ui;
    std::unique_ptr<IConsoleFactory> m_consoleFactory;
    std::unique_ptr<IPlotFactory> m_plotFactory;
    std::unique_ptr<QLabel> m_status;
    std::unique_ptr<IConsole> m_console;
    std::unique_ptr<ISettingsDialog> m_settings;
    std::unique_ptr<QSerialPort> m_serial;
    std::unique_ptr<QCustomPlot> m_customPlot;
    QByteArray m_data;
};

